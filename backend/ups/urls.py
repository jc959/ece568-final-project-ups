from django.conf.urls import include
from django.urls import path
from . import views
from rest_framework.routers import DefaultRouter

urlpatterns = [
    path("login", views.login_request, name="login"),
    path("register", views.register, name="register"),
    path("logout", views.logout_request, name="logout"),
    path("my_packages", views.get_packages, name="packages"),
    path("get_trucks", views.get_trucks, name="trucks")
]


