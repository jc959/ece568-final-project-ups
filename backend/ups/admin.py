from django.contrib import admin
from .models import Truck, Package, Item


class TruckAdmin(admin.ModelAdmin):
    list_display = ('driver', 'status', 'x', 'y')


class PackageAdmin(admin.ModelAdmin):
    list_display = ('user', 'x', 'y', 'truck')


class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'package')


admin.site.register(Truck, TruckAdmin)
admin.site.register(Package, PackageAdmin)
admin.site.register(Item, ItemAdmin)
