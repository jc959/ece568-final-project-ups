# Generated by Django 4.2 on 2023-04-19 13:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ups', '0002_packageitem_delete_item'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PackageItem',
            new_name='Item',
        ),
    ]
