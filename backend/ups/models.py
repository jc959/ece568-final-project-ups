from django.db import models
from django.contrib.auth.models import User

TRUCK_STATUS = (
    (1, 'idle'),
    (2, 'en route'),
    (3, 'arrive_warehouse'),
    (4, 'delivering')
)


class Truck(models.Model):
    driver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='truck_driver', null=True, blank=True)
    status = models.PositiveSmallIntegerField(choices=TRUCK_STATUS, null=False)
    x = models.FloatField()
    y = models.FloatField()


class Package(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='package_owner', null=True, blank=True)
    x = models.FloatField()
    y = models.FloatField()
    truck = models.ForeignKey(Truck, on_delete=models.CASCADE, related_name='packages', null=False)


class Item(models.Model):
    name = models.CharField(null=False)
    description = models.CharField(null=False)
    package = models.ForeignKey(Package, on_delete=models.CASCADE, related_name='items', null=False, blank=False)
