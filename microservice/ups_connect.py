import socket
import time
import world_ups_pb2
from socket import timeout as TimeOutError
from google.protobuf.internal.decoder import _DecodeVarint32
from google.protobuf.internal.encoder import _EncodeVarint
import amazon_ups_pb2
import requests

ip_addr = '152.3.53.130'
num_trucks = 5  # Number of trucks to initialize in the world
X = 0  # Starting x position for trucks
Y = 0  # Starting y position for trucks


# DEBUG: keep sending Amazon worldid before recv "success" from Amazon?
def ups_amzn_connect(ip, worldid):
    # create a socket object
    ups_amzn_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # set the IP address and port number of the world server
    # ip = '152.3.53.130'  # replace with the actual IP address of the world server
    port = 34567

    # connect to the amazon server
    connected = False

    while not connected:
        time.sleep(2)
        try:
            ups_amzn_socket.connect((ip, port))
            connected = True
        except ConnectionRefusedError:
            connected = False

    # create an UtoAzConnect message to send to the amazon server
    connect_msg = amazon_ups_pb2.UtoAzConnect()
    connect_msg.worldid = worldid

    # encode the UtoAzConnect message and send it to the world server
    encoded_msg = connect_msg.SerializeToString()
    _EncodeVarint(ups_amzn_socket.send, len(encoded_msg), None)
    ups_amzn_socket.settimeout(5.0)
    ups_amzn_socket.send(encoded_msg)

    # receive a message from Amazon
    var_int_buff = []
    while len(var_int_buff) == 0:
        try:
            while True:
                buf = ups_amzn_socket.recv(1)
                var_int_buff += buf
                msg_len, new_pos = _DecodeVarint32(var_int_buff, 0)
                if new_pos != 0:
                    break
            whole_msg = ups_amzn_socket.recv(msg_len)

            # parse the received message as an AzConnected message
            connected_msg = amazon_ups_pb2.AzConnected()
            connected_msg.ParseFromString(whole_msg)

            # print the received message
            print("AzConnected result: ", connected_msg.result)
            if connected_msg.result == 'fail':
                # Need to implement a reconnect to world server function
                pass

            return ups_amzn_socket
        except TimeOutError:
            print("UPS to Amazon timed out")
            ups_amzn_socket.send(encoded_msg)


def ups_world_connect(ip):
    # create a socket object
    ups_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # set the IP address and port number of the world server
    # ip = '127.0.0.1'  # replace with the actual IP address of the world server
    port = 12345
    connected = False
    while not connected:
        time.sleep(2)
        try:
            ups_socket.connect((ip, port))
            connected = True
        except ConnectionRefusedError:
            connected = False
    return ups_socket


def send_Uconnect(ups_socket):
    # create an UConnect message to send to the world server
    connect_msg = world_ups_pb2.UConnect()
    connect_msg.isAmazon = False  # set the isAmazon field to True
    trucks = []
    for i in range(1, num_trucks + 1):
        truck_msg = world_ups_pb2.UInitTruck()
        truck_msg.id = i
        truck_msg.x = X
        truck_msg.y = Y
        trucks.append(truck_msg)

    # connect_msg.trucks = trucks
    connect_msg.trucks.extend(trucks)
    # encode the AConnect message and send it to the world server
    encoded_msg = connect_msg.SerializeToString()
    _EncodeVarint(ups_socket.send, len(encoded_msg), None)
    ups_socket.settimeout(5.0)
    try:
        ups_socket.send(encoded_msg)
    except:
        print("timed out")

    # receive a message from the world server
    var_int_buff = []
    while len(var_int_buff) == 0:
        try:
            while True:
                buf = ups_socket.recv(1)
                var_int_buff += buf
                msg_len, new_pos = _DecodeVarint32(var_int_buff, 0)
                if new_pos != 0:
                    break
            whole_msg = ups_socket.recv(msg_len)
            connected_msg = world_ups_pb2.UConnected()
            connected_msg.ParseFromString(whole_msg)

            print(connected_msg)

            return connected_msg.worldid
        except TimeOutError:  # what error would this be if it randomly shut
            print("World did not send a response.")
            ups_socket.send(encoded_msg)
            # reconnect_to_world()

    # parse the received message as an UConnected message


def make_req():
    url = "http://web:8000/ups/get_trucks"
    response = requests.get(url)
    print(response.text)
    # json_response = json.loads(response.text)
    # print(json_response)


if __name__ == "__main__":
    # DEBUG: keep trying to send Uconnect to world server before recv from msg?
    # print("I GOT HERE")
    # make_req()
    ups_world_socket = ups_world_connect(ip_addr)
    worldid = send_Uconnect(ups_world_socket)

    ups_amzn_socket = ups_amzn_connect(ip_addr, worldid)
    # for testing on local machine #
    # ups_world_socket = ups_world_connect('127.0.0.1')
    # ups_amzn_socket = ups_amzn_connect('127.0.0.1')
