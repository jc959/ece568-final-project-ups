from django.shortcuts import render, redirect
from .forms import NewUserForm, UpdateUserForm
from .models import Truck, Package, Item, TRUCK_STATUS
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm


def homepage(request):
    if request.user.is_authenticated:
        owned_packages = Package.objects.filter(user=request.user)
        package_info = []
        for package in owned_packages:
            items = Item.objects.filter(package=package)
            package_info.append(
                (
                    package,
                    items,
                    TRUCK_STATUS.__getitem__(package.truck.status - 1)[1],
                ))
        context = {
            'packages': package_info,
        }
        return render(request, 'home.html', context=context)
    return redirect("login")


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("home")
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request, template_name="registration/login.html", context={"login_form": form})


def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("/ups/login")


def register(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid() and not User.objects.filter(email=form.cleaned_data.get('email')).exists():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("home")
        messages.error(request, "Unsuccessful registration. Invalid information.")
    form = NewUserForm()
    return render(request=request, template_name="registration/register.html", context={"register_form": form})


def update_user(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = UpdateUserForm(request.POST, instance=request.user)
            if form.is_valid():
                form.save()
                return redirect("home")
        form = UpdateUserForm(instance=request.user)
        return render(request=request, template_name="registration/update_user.html",
                      context={"update_user_form": form})
    return redirect("login")


def get_packages(request):
    if request.user.is_authenticated:
        owned_packages = Package.objects.filter(user=request.user)
        package_info = []
        for package in owned_packages:
            package_info.append((package, package.item, package.truck.status))
        context = {
            'packages': package_info,
        }
        print(package_info)
        return render(request, 'my_packages.html', context=context)
    return redirect("login")


def get_trucks(request):
    trucks = Truck.objects.all()
    context = {
        'trucks': trucks,
    }
    return render(request, 'trucks.html', context=context)
